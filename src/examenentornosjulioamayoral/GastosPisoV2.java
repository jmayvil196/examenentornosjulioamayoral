package examenEntornosJulioAMayoral;

import java.util.Date;

/**
 *
 * @author Julio Alberto Mayoral Villegas
 *
 * @version 1.0
 */
public class GastosPisoV2 {

    /**
     *
     * Constructor para una nueva cuenta con un saldo inicial, una fecha de
     * creación y un límite de crédito.
     *
     * @param personas Las personas que puedan acceder.
     *
     * @param euros el dinero, en euros, que tenemos.
     *
     * @param gastos los gastos que tendremos en nuestra cuenta.
     *
     * @param concepto el concepto del ingreso.
     * @param fecha tipo Date que nos indica el momento de la transacción.
     * @param contador para recorrer el array.
     */
    //Encapsulamos los atributos públicos
    private int personas;//persons
    private int euros;//euros

    //e y p no son nombres aclarativos. Mejor llamarlos personas y euros
    private int gastos[];
    private String concepto[];
    private Date fecha[];
    private int contador = 0;

    public int getEuros() {
        return euros;
    }

    public void setEuros(int euros) {
        this.euros = euros;
    }

    //En la clase no para de comprobarse que se no pueda iniciar con dinero negativo. Esa funcionalidad la podemos sacar en un
    //método y llamar después.
    
    /**
     * @void comprueba si el salgo es negativo
     */
    public void comprobarNegativo(int euros) {
        if ((euros < 0)) {
            throw new IllegalArgumentException("No se puede iniciar la clase con dinero negativo");
        }
    }

    /**
     * Constructor de la cuenta bancaria
     */
    
    //Creo un método sobrecargado por si necesitas acumular varias posiciones en un array o por si 
    //nos puede valer el dato sin más. Se pueden usar juntos incluso.
    public void cuentaBancaria(int gastos[], Date fecha[], String concepto[]) {

    }

    public void cuentaBancaria(int gastos, Date fecha, String concepto) {

    }

     /**
     * @void método que comprueba los gatos
     */
    public GastosPisoV2(int personas, int euros) throws IllegalArgumentException {

        comprobarNegativo(euros);

        if (personas < 2) {
            throw new IllegalArgumentException("No se puede iniciar la clase con menos de 2 personas");
        } else {
        }
        this.personas = personas;
        this.euros = euros;

        //Estos datos tienen más sentido como una clase aparte, donde se pidan
        //como parámetros datos, fecha y concepto.
        //gastos = new int[1000];
        //fecha = new Date[1000];
        //concepto = new String[1000];
        cuentaBancaria(gastos[1000], fecha[1000], concepto[1000]);
    }

     /**
     * @void método para ingresar dinero
     */
    public void ingresarDinero(int dinero, String notas) throws Exception {
        if (dinero < 0) {
            throw new Exception("El dinero no puede ser negativo");
        } else {
            this.euros += dinero;
        }
        //cuentaBancaria(dinero, fecha, notas);
        cuentaBancaria(gastos[contador], fecha[contador], concepto[contador]);
        contador++;
    }

     /**
     * @void método que lista los gastos
     */
    public void listaGastos(String motivo) {
        for (int i = 0; i < contador; i++) {
            if (concepto[i].indexOf(motivo) > -1) {
                if (gastos[i] > 0) {
                    System.out.println("---" + fecha[i] + "-" + concepto[i] + "-" + gastos[i]);
                }
            }
        }
    }
    
     /**
     * @void comprueba y lista los ingresos
     */
    public void ListarIngresos() {
        for (int i = 0; i < contador; i++) {
            if (gastos[i] < 0) {
                System.out.println("---" + fecha[i] + "-" + concepto[i] + "-" + gastos[i]);
            }
        }
    }
    
     /**
     * @void aquí se anotan los gatos
     */
    public void anotarGasto(int euros, String motivo, Date dia) throws IllegalArgumentException {

        comprobarNegativo(euros);

        cuentaBancaria(euros, dia, motivo);
        this.euros -= euros;
        contador++;
    }
    
    public static void main(String[] args) {
        GastosPisoV2 g = new GastosPisoV2(5, 100);
    }
}
