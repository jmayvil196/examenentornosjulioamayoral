/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package examenentornosjulioamayoral;

import examenEntornosJulioAMayoral.GastosPisoV2;
import java.util.Date;
import jdk.jshell.execution.Util;
import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author usuario_mañana
 */
public class GastosPisoV2Test {
    
    public GastosPisoV2Test() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    void testGetEuros() {
        System.out.println("getEuros");
        GastosPisoV2 instance = null;
        int expResult = 0;
        int result = instance.getEuros();
        assertEquals(expResult, result);
    }

    @Test
    void testSetEuros() {
        System.out.println("setEuros");
        int euros = 0;
        GastosPisoV2 instance = null;
        instance.setEuros(euros);
    }

    @Test
    void testComprobarNegativo() {
        System.out.println("comprobarNegativo");
        int euros = 0;
        GastosPisoV2 instance = null;
        instance.comprobarNegativo(euros);
    }

    @Test
    void testCuentaBancaria_3args_1() {
        System.out.println("cuentaBancaria");
        int[] gastos = null;
        Date[] fecha = null;
        String[] concepto = null;
        GastosPisoV2 instance = null;
        instance.cuentaBancaria(gastos, fecha, concepto);
    }

    @Test
    void testCuentaBancaria_3args_2() {
        System.out.println("cuentaBancaria");
        int gastos = 0;
        Date fecha = null;
        String concepto = "";
        GastosPisoV2 instance = null;
        instance.cuentaBancaria(gastos, fecha, concepto);
    }

    @Test
    void testIngresarDinero() throws Exception {
        System.out.println("ingresarDinero");
        int dinero = 0;
        String notas = "";
        GastosPisoV2 instance = null;
        instance.ingresarDinero(dinero, notas);
    }

    @Test
    void testListaGastos() {
        System.out.println("listaGastos");
        String motivo = "";
        GastosPisoV2 instance = null;
        instance.listaGastos(motivo);
    }

    @Test
    void testListarIngresos() {
        System.out.println("ListarIngresos");
        GastosPisoV2 instance = null;
        instance.ListarIngresos();
    }

    @Test
    void testAnotarGasto() {
        System.out.println("anotarGasto");
        int euros = 0;
        String motivo = "";
        Date dia = null;
        GastosPisoV2 instance = null;
        instance.anotarGasto(euros, motivo, dia);
    }

    @Test
    void testMain() {
        System.out.println("main");
        String[] args = null;
        GastosPisoV2.main(args);
    }

}
